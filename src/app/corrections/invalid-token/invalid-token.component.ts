import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-invalid-token',
  templateUrl: './invalid-token.component.html',
  styleUrls: ['./invalid-token.component.sass']
})
export class InvalidTokenComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignDcsComponent } from './assign-dcs.component';

describe('AssignDcsComponent', () => {
  let component: AssignDcsComponent;
  let fixture: ComponentFixture<AssignDcsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignDcsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignDcsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
